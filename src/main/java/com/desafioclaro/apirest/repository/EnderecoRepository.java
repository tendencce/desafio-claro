package com.desafioclaro.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desafioclaro.apirest.entity.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
