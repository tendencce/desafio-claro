package com.desafioclaro.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desafioclaro.apirest.entity.Contato;

public interface ContatoRepository extends JpaRepository<Contato, String> {

}
