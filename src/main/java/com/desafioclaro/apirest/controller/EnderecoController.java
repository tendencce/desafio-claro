package com.desafioclaro.apirest.controller;

import com.desafioclaro.apirest.entity.Endereco;
import com.desafioclaro.apirest.service.impl.ConsultaCepServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/endereco")
@RequiredArgsConstructor
public class EnderecoController {

    private final ConsultaCepServiceImpl service;

    @GetMapping("/{cep}")
    public Optional<Endereco> getByCep(@PathVariable String cep) throws Exception {
        return Optional.ofNullable(service.findByCep(cep));
    }
}
