package com.desafioclaro.apirest.service;

import com.desafioclaro.apirest.entity.Endereco;

public interface ConsultaCepService {
    Endereco findByCep(String cep) throws Exception;
}
