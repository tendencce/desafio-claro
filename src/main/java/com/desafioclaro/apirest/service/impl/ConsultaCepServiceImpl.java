package com.desafioclaro.apirest.service.impl;

import com.desafioclaro.apirest.entity.Endereco;
import com.desafioclaro.apirest.service.ConsultaCepService;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import static com.desafioclaro.apirest.util.ConverteJsonEmString.converteJsonEmString;

@Service
public class ConsultaCepServiceImpl implements ConsultaCepService {

    static String api = "https://ws.apicep.com/cep/";
    static int sucessCode = 200;

    @Override
    public Endereco findByCep(String cep) throws Exception {

        String apiCallUrl = api + cep + ".json";

        try {
            URL url = new URL(apiCallUrl);
            HttpURLConnection conecttion = (HttpURLConnection) url.openConnection();

            if (conecttion.getResponseCode() != sucessCode)
                throw new RuntimeException("HTTP error code: " + conecttion.getResponseCode());

            BufferedReader response = new BufferedReader(new InputStreamReader((conecttion.getInputStream())));
            String jsonEmString = converteJsonEmString(response);

            Gson gson = new Gson();
            Endereco endereco = gson.fromJson(jsonEmString, Endereco.class);

            return (endereco);
        } catch (Exception e) {
            throw new Exception("Error: " + e);
        }
    }
}
